package org.pgu.tree;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Node {

    private String key;
    private Node leftChild;
    private Node rightChild;

    public Node(String key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return "Ключ: " + key;
    }

}
