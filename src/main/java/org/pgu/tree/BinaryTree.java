package org.pgu.tree;


import java.util.LinkedList;
import java.util.Queue;

public class BinaryTree<T> {

    private Node root;

    public void insert(String key) {
        Node current = root;
        if (lookup(key) != null) {
            System.err.println("Данный ключ уже существует");
            return;
        }
        Node parent;
        Node newNode = new Node(key);
        if (root == null)
            root = newNode;
        else {
            while (true) {
                parent = current;
                if (key.compareTo(current.getKey()) < 0) {
                    current = current.getLeftChild();
                    if (current == null) {
                        parent.setLeftChild(newNode);
                        return;
                    }
                } else {
                    current = current.getRightChild();
                    if (current == null) {
                        parent.setRightChild(newNode);
                        return;
                    }
                }
            }
        }
    }

    public Node lookup(String key) {
        Node current = root;
        if (current == null) return null;
        while (current.getKey().compareTo(key) != 0) {
            if (key.compareTo(current.getKey()) < 0)
                current = current.getLeftChild();
            else
                current = current.getRightChild();
            if (current == null)
                return null;
        }
        return current;
    }

    public void prettyPrint(Node node) {
        // get height first
        int height = heightRecursive(node);

        // perform  level order traversal
        Queue<Node> queue = new LinkedList<Node>();

        int level = 0;
        final int SPACE = 6;
        int nodePrintLocation = 0;

        // special node for pushing when a node has no getLeftChild() or getRightChild() child (assumption, say this node is a node with value Integer.MIN_VALUE)
        Node special = new Node();

        queue.add(node);
        queue.add(null);    // end of level 0
        while (!queue.isEmpty()) {
            node = queue.remove();

            if (node == null) {
                if (!queue.isEmpty()) {
                    queue.add(null);
                }

                // start of new level
                System.out.println();
                level++;
            } else {
                nodePrintLocation = ((int) Math.pow(2, height - level)) * SPACE;

                System.out.print(getPrintLine(node, nodePrintLocation));

                if (level < height) {
                    // only go till last level
                    queue.add((node.getLeftChild() != null) ? node.getLeftChild() : special);
                    queue.add((node.getRightChild() != null) ? node.getRightChild() : special);
                }
            }
        }
    }

    public void prettyPrint() {
        prettyPrint(root);
    }

    private String getPrintLine(Node node, int spaces) {
        StringBuilder sb = new StringBuilder();

        if (String.valueOf(Integer.MIN_VALUE).equals(node.getKey())) {
            // for child nodes, print spaces
            for (int i = 0; i < 2 * spaces; i++) {
                sb.append(" ");
            }

            return sb.toString();
        }

        int i = 0;
        int to = spaces / 2;

        for (; i < to; i++) {
            sb.append(' ');
        }
        to += spaces / 2;
        char ch = ' ';
        if (node.getLeftChild() != null) {
            ch = '_';
        }
        for (; i < to; i++) {
            sb.append(ch);
        }

        String value = node.getKey();
        sb.append(value);

        to += spaces / 2;
        ch = ' ';
        if (node.getRightChild() != null) {
            ch = '_';
        }
        if (node.getKey() != null) {
            for (i += value.length(); i < to; i++) {
                sb.append(ch);
            }
        }


        to += spaces / 2;
        for (; i < to; i++) {
            sb.append(' ');
        }

        return sb.toString();
    }

    private int heightRecursive(Node node) {
        if (node == null) {
            // empty tree
            return -1;
        }

        if (node.getLeftChild() == null && node.getRightChild() == null) {
            // leaf node
            return 0;
        }

        return 1 + Math.max(heightRecursive(node.getLeftChild()), heightRecursive(node.getRightChild()));
    }

}
