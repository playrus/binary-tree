package org.pgu.tree;

import java.util.Scanner;

public class App {

    private static BinaryTree<String> binaryTree = new BinaryTree<>();

    private static String commandList =
                    "1 - добавить значение.\n" +
                    "2 - найти значение\n" +
                    "3 - вывести бинарное дерево.\n" +
                    "4 - выход.";

    private static void process() {
        while (true) {
            System.out.println("Выберете команду");
            System.out.println(commandList);
            System.out.println();
            final Scanner scanner = new Scanner(System.in);
            final String command = scanner.nextLine();
            checkCommand(command);
            System.out.println();
        }
    }

    private static void insertDateCommand(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите ключ:");
        final String key = sc.nextLine();
        binaryTree.insert(key);
    }

    private static void findDateCommand(){
        final Scanner sc = new Scanner(System.in);
        System.out.println("Введите ключ:");
        final String key = sc.nextLine();
        Node foundNode = binaryTree.lookup(key);
        if (foundNode == null) {
            System.err.println("Ключ не найден");
            return;
        }
        System.out.println(foundNode.toString());
    }

    private static void printTreeCommand(){
        binaryTree.prettyPrint();
    }


    private static void checkCommand(final String command) {
            switch (command) {
                case "1":
                    insertDateCommand();
                    break;
                case "2":
                    findDateCommand();
                    break;
                case "3":
                    printTreeCommand();
                    break;
                case "4":
                    System.exit(0);
                default:
                    System.err.println("Ошибка ввода");
                    break;
            }
        }

    public static void main(String[] args) {
        process();
    }
}
